
#include <stdio.h>
#include <pthread.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include<math.h>

 void *CalcularPI(void* limite_inf_Ptr);
 double pi=0.0;
 double pi_temp[1000];
 pthread_mutex_t lock;
int numThreads;
int main(int argc, char const *argv[]){
  clock_t begin = clock();
 numThreads=argc;

  int ctr[numThreads];
  //Crea la cantidad de threads que se van a usar en un arreglo
  pthread_t inc_x_thread[numThreads];

  for(int j=0;j<numThreads;j++){
    ctr[j]=j;
    //LLama a la funcion calcular pi para cada thread mandando el contador j
    pthread_create(&inc_x_thread[j], NULL, &CalcularPI, &ctr[j]);
  }

for(int j=0;j<numThreads;j++){
  //une todos los resultados
  pthread_join(inc_x_thread[j], NULL);
}
pthread_mutex_destroy(&lock);
printf(" valor final p:%f \n", pi);
clock_t end = clock();
double time_spent = (double)(end - begin) / CLOCKS_PER_SEC;
printf(" tiempo  de ejecucion :%f \n", time_spent);
return 0;
}

void *CalcularPI(void* limite_inf_Ptr){


  int *limite_inf=(int *)limite_inf_Ptr;
  if(numThreads<4){//Si el programa tiene pocos threads usa esta aprte para asegurarse confianza en el valor, ya que cada thread realiza mas iteraciones.
  for(int i=(*limite_inf)*(1000000);i<((*limite_inf)+(1000000));i++){
  //  int signo=pow(-1,i);
    //pi+=(4.0*((double)signo/(2.0*(double)i+1.00)));
    pi_temp[(*limite_inf)]+=(4.0*((double)pow(-1,i)/(2.0*(double)i+1.00)));
    }
  }
  else {//Entre mas threads menos repeticiones se ocupan para optener el mejor resultado
    for(int i=(*limite_inf)*(10);i<((*limite_inf)+(10));i++){
      //int signo=pow(-1,i);
      pi_temp[(*limite_inf)]+=(4.0*((double)pow(-1,i)/(2.0*(double)i+1.00)));
      }


  }
    //Le hace lock al codigo para que no los modifique alguien mas
  pthread_mutex_lock(&lock);
  pi=pi+pi_temp[(*limite_inf)];
  pthread_mutex_unlock(&lock);
  return NULL;
}
