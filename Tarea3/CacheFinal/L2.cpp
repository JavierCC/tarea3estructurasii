#include "L2.h"
#include <bitset>
#include <sstream>
#include "Campo1.h"


L2::L2() {
	Misses = 0;
	for (int i = 0; i < 4096; i++)
	{
		CacheMatrix[i] = new Campo2();

	}
}

/*
Función que victimiza un bloque en la caché de segundo nivel. Recibe como parámetro un puntero al bloque que se sacó de 
una caché de primer nivel para ser almacenado. 
*/
void L2::Evict(Campo1* Evicted) {
	int IndexData1, TagData1, OffsetData1;
	if (Evicted!=nullptr) {
		long long int IOI1 = Evicted->IOI;
		int TagMask1, IndexMask1, OffsetMask1;
		TagMask1 = 4294836224;
		IndexMask1 = 131040;
		OffsetMask1 = 31;
		TagData1 = IOI1 & TagMask1;
		TagData1 >>= 13;
		IndexData1 = IOI1 & IndexMask1;
		IndexData1 >>= 5;
		OffsetData1 = IOI1 & OffsetMask1;

		CacheMatrix[IndexData]->Tag = TagData;
	}
	
}
/*
Función que determina si un bloque se encuentra en la caché de segundo nivel. Si el bloque se encuentra, se devuelve true,
por el contrario, si no se encuentra el bloque, se retorna false. 
*/
bool L2::L2Check() {
	char InstruType = RcvdLine.at(2);
	/*int OffsetData, IndexData, TagData;
	string OnlyInstru = RcvdLine.substr(4, 8);
	stringstream ss;
	ss << hex << OnlyInstru;
	unsigned n;
	ss >> n;
	bitset<32> Converted(n);
	bitset<32> OffMask(string("00000000000000000000000000011111"));
	bitset<32> OD = Converted & OffMask;
	bitset<32> TagMask(string("11111111111111100000000000000000"));
	bitset<32> TD = Converted & TagMask;
	TD >>= 13;
	bitset<32> IndexMask(string("00000000000000011111111111100000"));
	bitset<32> ID = Converted & IndexMask;
	ID >>= 5;
	OffsetData = OD.to_ulong();
	IndexData = ID.to_ulong();
	TagData = TD.to_ulong();*/


	if (InstruType == '0') {
		if (this->CacheMatrix[IndexData]->Tag == TagData) {
			return true;
		}
		else{
			return false;
		}
	}
	else {
		if (CacheMatrix[IndexData]->Tag == TagData) {
			return true;
		}
		else {
			CacheMatrix[IndexData]->Tag = TagData;
			return false;
		}
	}


}