#pragma once
#include "L1.h"
#include "L2.h"

class Bus {
public:
	Bus();

	int LineCount;
	void CheckBusOnRead(L1* Local, L1* Remote,int i, int HitCol, int RemoteState);
	void CheckBusOnWrite(L1* Local, L1* Remote, int i, int HitCol, int RemoteState);

	L1* Proce1;
	L1* Proce2;
	L2* SharedCache;
	string ReadLine;
	void Compare();


};