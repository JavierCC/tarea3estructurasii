#include "L1.h"

L1::L1(L2* L2Ptr) {
	this->L2Ptr = L2Ptr;
	Invalidtaions = 0;
	Misses = 0;
	for (int i = 0; i < 256; i++)
	{
		for (int j = 0; j < 2; j++)
		{
			CacheMatrix[i][j] = nullptr;

		}
	}
}

/*
Función que realiza el eviction de un bloque en una caché de primer nivel. Devuelve un puntero al bloque que fue victimizado.
Posee controles de memoria para evitar intentar acceder posiciones vacías o sin inicializar, y realiza los reemplazos
basándose en la política de reemplazo LRU.
*/
Campo1* L1::Evict() {
	char InstruType = RcvdLine.at(2);
	int BothNull=0;
	int NullSpace;
	for (int i = 0; i < 2; i++) {
		if (CacheMatrix[IndexData][i] == nullptr) {
			BothNull++;
			NullSpace = i;
		}
	}

	if (BothNull == 0) {
		Campo1* temp=nullptr;
		for (int i = 0; i < 2; i++)
		{
			if (CacheMatrix[IndexData][i]->LRU == true) {
				temp = CacheMatrix[IndexData][i];
				CacheMatrix[IndexData][i]->Tag = TagData;
				CacheMatrix[IndexData][i]->DB = 1;
				CacheMatrix[IndexData][i]->LRU = false;
				CacheMatrix[IndexData][i]->IOI = IOI;
				if (InstruType == '0') { CacheMatrix[IndexData][i]->Coherence = 4; }
				else { CacheMatrix[IndexData][i]->Coherence = 1; }
				if (i == 0) {
					CacheMatrix[IndexData][i + 1]->LRU = true;
				}
				else {
					CacheMatrix[IndexData][i - 1]->LRU = true;
				}
			}
		}
		return temp;

	}
	else if (BothNull==1) {
		if (NullSpace == 0) {
			CacheMatrix[IndexData][NullSpace] = new Campo1();
			CacheMatrix[IndexData][NullSpace]->Tag = TagData;
			CacheMatrix[IndexData][NullSpace]->LRU = false;
			CacheMatrix[IndexData][NullSpace]->IOI = IOI;
			CacheMatrix[IndexData][NullSpace+1]->LRU = true;
			if (InstruType == '0') { CacheMatrix[IndexData][NullSpace]->Coherence = 4; }
			else { CacheMatrix[IndexData][NullSpace]->Coherence = 1; }
			return nullptr;
		}
		else {
			CacheMatrix[IndexData][NullSpace] = new Campo1();
			CacheMatrix[IndexData][NullSpace]->Tag = TagData;
			CacheMatrix[IndexData][NullSpace]->LRU = false;
			CacheMatrix[IndexData][NullSpace]->IOI = IOI;
			if (InstruType == '0') { CacheMatrix[IndexData][NullSpace-1]->Coherence = 4; }
			else { CacheMatrix[IndexData][NullSpace]->Coherence = 1; }
			return nullptr;
		}
	}
	else {
		CacheMatrix[IndexData][0] = new Campo1();
		CacheMatrix[IndexData][0]->Tag = TagData;
		CacheMatrix[IndexData][0]->LRU = false;
		CacheMatrix[IndexData][0]->Coherence = 4;
		CacheMatrix[IndexData][0]->IOI = IOI;
		return nullptr;
	}
}

