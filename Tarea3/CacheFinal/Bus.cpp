#pragma once
#include  "Bus.h"
#include <iostream>

Bus::Bus() {
	LineCount = 0;
	SharedCache = new L2();
	Proce1 = new L1(SharedCache);
	Proce2 = new L1(SharedCache);
}

/*void Bus::CheckBusOnWrite(L1* Local, L1* Remote, int i, int HitCol, int RemoteState) {
	if (Local->CacheMatrix[Local->IndexData][i]->Coherence==4 || Local->CacheMatrix[Local->IndexData][i]->Coherence==3) {
		Local->CacheMatrix[Local->IndexData][i]->Coherence = 2;
	}
	else if (Local->CacheMatrix[Local->IndexData][i]->Coherence==2) {
		Local->CacheMatrix[Local->IndexData][i]->Coherence = 1;
	}
	if ((RemoteState == 1 || RemoteState == 2 || RemoteState == 3)&&RemoteState!=5) {
		Remote->CacheMatrix[Remote->IndexData][HitCol]->Coherence = 4;
		Remote->Invalidtaions++;
	}

	int EstadoFinal1 = Local->CacheMatrix[Local->IndexData][i]->Coherence;
	int EstadoFinal2;
	if (RemoteState==5) {
		EstadoFinal2 = -1;
	}
	else {
		EstadoFinal2 = Remote->CacheMatrix[Remote->IndexData][HitCol]->Coherence;
	}
	cout << "Local: " << EstadoFinal1 ;
	cout << "Remoto: " << EstadoFinal2 << "\n";

}

void Bus::CheckBusOnRead(L1* Local, L1* Remote, int i, int HitCol, int RemoteState) {
	if (Local->CacheMatrix[Local->IndexData][i]->Coherence==4) {
		if (RemoteState == 2 || RemoteState == 1) {
			Local->CacheMatrix[Local->IndexData][i]->Coherence=3;
		}
		else if(RemoteState==4 || RemoteState==5) {
			Local->CacheMatrix[Local->IndexData][i]->Coherence = 2;
		}
	}
	if ((RemoteState == 1 || RemoteState == 2)&&RemoteState!=5) {
		Remote->CacheMatrix[Remote->IndexData][HitCol]->Coherence = 3;
	}

	
	int EstadoFinal1 = Local->CacheMatrix[Local->IndexData][i]->Coherence;
	int EstadoFinal2;
	if (RemoteState == 5) {
		EstadoFinal2 = -1;
	}
	else {
		EstadoFinal2 = Remote->CacheMatrix[Remote->IndexData][HitCol]->Coherence;
	}
	cout << "Local: " << EstadoFinal1;
	cout << "Remoto: " << EstadoFinal2 << "\n";
}*/

/*
Función que chequea el estado de MESI cuando se realiza una lectura. Recibe un puntero al procesador 
Local(el que realiza la solicitud), otro al procesador remoto, el número de columna en el que se encuentra el dato
en el procesador local, el número de columna en la que se encuentra el dato en el proceador remoto, y el estado
del bloque en el procesador remoto.
*/
void Bus::CheckBusOnRead(L1* Local, L1* Remote, int i, int HitCol, int RemoteState) {
	switch (Local->CacheMatrix[Local->IndexData][i]->Coherence) {
	case 1:
		Local->CacheMatrix[Local->IndexData][i]->Coherence = 1;
		break;
	case 2:
		Local->CacheMatrix[Local->IndexData][i]->Coherence = 2;
		break;
	case 3:
		Local->CacheMatrix[Local->IndexData][i]->Coherence = 3;
		break;
	case 4:
		switch (RemoteState) {
		case 1:
			Local->CacheMatrix[Local->IndexData][i]->Coherence = 3;
			Remote->CacheMatrix[Remote->IndexData][HitCol]->Coherence = 3;
			break;
		case 2:
			Local->CacheMatrix[Local->IndexData][i]->Coherence = 3;
			Remote->CacheMatrix[Remote->IndexData][HitCol]->Coherence = 3;
			break;
		case 3:
			break;
		case 4:
			Local->CacheMatrix[Local->IndexData][i]->Coherence = 2;
			break;
		case 5:
			Local->CacheMatrix[Local->IndexData][i]->Coherence = 2;
			break;
		}
		break;
	}
	switch (RemoteState) {
	case 1:
		Remote->CacheMatrix[Remote->IndexData][HitCol]->Coherence = 3;
		break;
	case 2:
		Remote->CacheMatrix[Remote->IndexData][HitCol]->Coherence = 3;
		break;
	case 3:
		Remote->CacheMatrix[Remote->IndexData][HitCol]->Coherence = 3;
		break;
	case 4:
		Remote->CacheMatrix[Remote->IndexData][HitCol]->Coherence = 4;
		break;
	case 5:
		break;
	}

	int EstadoFinal1 = Local->CacheMatrix[Local->IndexData][i]->Coherence;
	int EstadoFinal2;
	if (RemoteState == 5) {
		EstadoFinal2 = -1;
	}
	else {
		EstadoFinal2 = Remote->CacheMatrix[Remote->IndexData][HitCol]->Coherence;
	}
	/*cout << "Local: " << EstadoFinal1;
	cout << "Remoto: " << EstadoFinal2 << "\n";*/
}
/*
Función que chequea el estado de MESI cuando se realiza una escritura. Recibe un puntero al procesador 
Local(el que realiza la solicitud), otro al procesador remoto, el número de columna en el que se encuentra el dato
en el procesador local, el número de columna en la que se encuentra el dato en el proceador remoto, y el estado
del bloque en el procesador remoto.
*/
void Bus::CheckBusOnWrite(L1* Local, L1* Remote, int i, int HitCol, int RemoteState) {
	switch (Local->CacheMatrix[Local->IndexData][i]->Coherence) {
	case 1:
		Local->CacheMatrix[Local->IndexData][i]->Coherence = 1;
		break;
	case 2:
		Local->CacheMatrix[Local->IndexData][i]->Coherence = 1;
		break;
	case 3:
		Local->CacheMatrix[Local->IndexData][i]->Coherence = 1;
		break;
	case 4:
		Local->CacheMatrix[Local->IndexData][i]->Coherence = 1;
		break;
	}

	switch (RemoteState) {
	case 1:
		Remote->CacheMatrix[Remote->IndexData][HitCol]->Coherence = 4;
		Remote->Invalidtaions++;
		break;
	case 2:
		Remote->CacheMatrix[Remote->IndexData][HitCol]->Coherence = 4;
		Remote->Invalidtaions++;
		break;
	case 3:
		Remote->CacheMatrix[Remote->IndexData][HitCol]->Coherence = 4;
		Remote->Invalidtaions++;
		break;
	case 4:
		Remote->CacheMatrix[Remote->IndexData][HitCol]->Coherence = 4;
		Remote->Invalidtaions++;
		break;
	case 5:
		break;
	}

	int EstadoFinal1 = Local->CacheMatrix[Local->IndexData][i]->Coherence;
	int EstadoFinal2;
	if (RemoteState == 5) {
		EstadoFinal2 = -1;
	}
	else {
		EstadoFinal2 = Remote->CacheMatrix[Remote->IndexData][HitCol]->Coherence;
	}
	/*cout << "Local: " << EstadoFinal1;
	cout << "Remoto: " << EstadoFinal2 << "\n";*/

}



/*
Función que implementa la funcionalidad principal del código. Realiza el parseo de las instrucciones para cada nivel de cache,
determina si es una instrucción del procesador 1 o 2, revisa si es un hit o un miss, realiza los llamados a la funciones de 
chequeo de coherencia, realiza los llamados a las funciones de eviction, y asigna los parámetros para obtener los resultados de
la simulación.
*/
void Bus::Compare(){
	char InstruType = ReadLine.at(2);

	//Parseo de la instruccion para L1
	int IndexData, TagData, OffsetData;
	string OnlyInstru = ReadLine.substr(4, 8);
	long long int IOI = stoi(OnlyInstru, 0, 16);
	int TagMask, IndexMask, OffsetMask;
	TagMask = 4294959104;
	IndexMask = 8160;
	OffsetMask = 31;
	TagData = IOI & TagMask;
	TagData >>= 13;
	IndexData = IOI & IndexMask;
	IndexData >>= 5;
	OffsetData = IOI & OffsetMask;
	Proce1->IndexData = IndexData;
	Proce1->OffsetData = OffsetData;
	Proce1->TagData = TagData;
	Proce2->IndexData = IndexData;
	Proce2->OffsetData = OffsetData;
	Proce2->TagData = TagData;
	Proce1->IOI = IOI;
	Proce2->IOI = IOI;

	//Parseo de la instruccion para L2
	int IndexData1, TagData1, OffsetData1;
	long long int IOI1 = stoi(OnlyInstru, 0, 16);
	unsigned int TagMask1;
	int IndexMask1, OffsetMask1;
	TagMask1 = 4294836224;
	IndexMask1 = 131040;
	OffsetMask1 = 31;
	TagData1 = IOI1 & TagMask1;
	TagData1 >>= 13;
	SharedCache->TagData = TagData1;
	IndexData1 = IOI1 & IndexMask1;
	IndexData1 >>= 5;
	SharedCache->IndexData = IndexData1;
	OffsetData1 = IOI1 & OffsetMask1;
	SharedCache->OffsetData = OffsetData;
	Proce1->RcvdLine = ReadLine;
	Proce2->RcvdLine = ReadLine;
	SharedCache->RcvdLine = ReadLine;

	bool LoadHit = false;
	bool StoreHit = false;

	//Instruccion del proce 1
	if (LineCount % 4 == 0) {
		Proce1->Accesses++;
		int E2;
		int HitCol2 = 2;
		for (int i = 0; i < 2; i++)
		{

			if (Proce2->CacheMatrix[IndexData][i] != nullptr) {
				if (Proce2->CacheMatrix[IndexData][i]->Tag == TagData) {
					E2 = Proce2->CacheMatrix[IndexData][i]->Coherence;
					HitCol2 = i;
				}
			}

		}

		if (HitCol2 == 2) {
			E2 = 5;
		}
		
		if (InstruType == '0') {
			
			for (int i = 0; i < 2; i++)
			{
				if (Proce1->CacheMatrix[IndexData][i] != nullptr) {
					if (Proce1->CacheMatrix[IndexData][i]->Tag == TagData) {
						LoadHit = true;
						Proce1->CacheMatrix[IndexData][i]->LRU = false;
						if (i == 0 && Proce1->CacheMatrix[IndexData][i + 1] != nullptr) {
							Proce1->CacheMatrix[IndexData][i + 1]->LRU = true;
						}
						else if(i==1 && Proce1->CacheMatrix[IndexData][i + 1] != nullptr){
							Proce1->CacheMatrix[IndexData][i - 1]->LRU = true;
						}
						CheckBusOnRead(Proce1, Proce2, i,HitCol2,E2);
						break;
					}
				}		
			}
			if (!LoadHit) {
				Proce1->Misses++;
				bool ReadHit = SharedCache->L2Check();
				if (ReadHit == false) {
					SharedCache->Misses++;
					Campo1* Evicted=Proce1->Evict();
					SharedCache->CacheMatrix[SharedCache->IndexData]->Tag = SharedCache->TagData;
					SharedCache->CacheMatrix[SharedCache->IndexData]->Coherence = 4;
					if (Evicted != nullptr) {
						SharedCache->Evict(Evicted);
					}
					for (int i = 0; i < 2; i++)
					{
						if (Proce1->CacheMatrix[IndexData][i] != nullptr) {
							if (Proce1->CacheMatrix[IndexData][i]->Tag == TagData) {
								CheckBusOnRead(Proce1, Proce2, i, HitCol2, E2);
								break;
							}
						}
					}
				}
				else {
					Campo1* Evicted = Proce1->Evict();
					if (Evicted != nullptr) {
						SharedCache->Evict(Evicted);
					}
					for (int i = 0; i < 2; i++)
					{
						if (Proce1->CacheMatrix[IndexData][i] != nullptr) {
							if (Proce1->CacheMatrix[IndexData][i]->Tag == TagData) {
								CheckBusOnRead(Proce1, Proce2, i, HitCol2, E2);
								break;
							}
						}
					}
				}
			}
		}
		else {
			for (int i = 0; i < 2; i++)
			{
				if (Proce1->CacheMatrix[IndexData][i]!=nullptr) {
					if (Proce1->CacheMatrix[IndexData][i]->Tag == TagData) {
						StoreHit = true;
						Proce1->CacheMatrix[IndexData][i]->LRU = false;
						if (i == 0 && Proce1->CacheMatrix[IndexData][i + 1] != nullptr) {
							Proce1->CacheMatrix[IndexData][i + 1]->LRU = true;
						}
						else if (i == 1 && Proce1->CacheMatrix[IndexData][i - 1] != nullptr) {
							Proce1->CacheMatrix[IndexData][i - 1]->LRU = true;
						}
						CheckBusOnWrite(Proce1,Proce2,i,HitCol2,E2);
						break;
					}
				}
			}
			if (!StoreHit) {
				Proce1->Misses++;
				bool WriteHit = SharedCache->L2Check();
				if (WriteHit == false) {
					SharedCache->Misses++;
					Campo1* Evicted = Proce1->Evict();
					SharedCache->CacheMatrix[SharedCache->IndexData]->Tag = SharedCache->TagData;
					SharedCache->CacheMatrix[SharedCache->IndexData]->Coherence = 4;
					if (Evicted != nullptr) {
						SharedCache->Evict(Evicted);
					}
					for (int i = 0; i < 2; i++)
					{
						if (Proce1->CacheMatrix[IndexData][i] != nullptr) {
							if (Proce1->CacheMatrix[IndexData][i]->Tag == TagData) {
								CheckBusOnWrite(Proce1, Proce2, i, HitCol2, E2);
								break;
							}
						}
					}
				}
				else {
					Campo1* Evicted = Proce1->Evict();
					if (Evicted != nullptr) {
						SharedCache->Evict(Evicted);
					}
					for (int i = 0; i < 2; i++)
					{
						if (Proce1->CacheMatrix[IndexData][i] != nullptr) {
							if (Proce1->CacheMatrix[IndexData][i]->Tag == TagData) {
								CheckBusOnWrite(Proce1, Proce2, i, HitCol2, E2);
								break;
							}
						}
					}
				}
			}

		}
	}
	else {
		Proce2->Accesses++;

		int E1;
		int HitCol1 = 2;
		for (int i = 0; i < 2; i++)
		{
			if (Proce1->CacheMatrix[IndexData][i] != nullptr) {
				if (Proce1->CacheMatrix[IndexData][i]->Tag == TagData) {
					E1 = Proce1->CacheMatrix[IndexData][i]->Coherence;
					HitCol1 = i;
				}
			}
		}

		if (HitCol1 == 2) {
			E1 = 5;
		}

		if (InstruType == '0') {
			for (int i = 0; i < 2; i++)
			{
				if (Proce2->CacheMatrix[IndexData][i] != nullptr) {
					if (Proce2->CacheMatrix[IndexData][i]->Tag == TagData) {
						LoadHit = true;
						Proce2->CacheMatrix[IndexData][i]->LRU = false;
						if (i == 0 && Proce2->CacheMatrix[IndexData][i + 1]!=nullptr) {
							Proce2->CacheMatrix[IndexData][i + 1]->LRU = true;
						}
						else if(i==1 && Proce2->CacheMatrix[IndexData][i + 1] != nullptr){
							Proce2->CacheMatrix[IndexData][i - 1]->LRU = true;
						}
						CheckBusOnRead(Proce2, Proce1, i, HitCol1, E1);
						break;
					}
				}
			}
			if (!LoadHit) {
				Proce2->Misses++;
				bool ReadHit = SharedCache->L2Check();
				if (ReadHit == false) {
					SharedCache->Misses++;
					SharedCache->CacheMatrix[SharedCache->IndexData]->Tag = SharedCache->TagData;
					SharedCache->CacheMatrix[SharedCache->IndexData]->Coherence = 4;
					Campo1* Evicted = Proce2->Evict();
					if (Evicted!=nullptr) {
						SharedCache->Evict(Evicted);
					}
					for (int i = 0; i < 2; i++)
					{
						if (Proce2->CacheMatrix[IndexData][i] != nullptr) {
							if (Proce2->CacheMatrix[IndexData][i]->Tag == TagData) {
								CheckBusOnRead(Proce2, Proce1, i, HitCol1, E1);
								break;
							}
						}
					}
					
				}
				else {
					Campo1* Evicted = Proce2->Evict();
					if (Evicted != nullptr) {
						SharedCache->Evict(Evicted);
					}
					for (int i = 0; i < 2; i++)
					{
						if (Proce2->CacheMatrix[IndexData][i] != nullptr) {
							if (Proce2->CacheMatrix[IndexData][i]->Tag == TagData) {
								CheckBusOnRead(Proce2, Proce1, i, HitCol1, E1);
								break;
							}
						}
					}
				}
			}
		}
		else {
			for (int i = 0; i < 2; i++)
			{
				if (Proce2->CacheMatrix[IndexData][i] != nullptr) {
					if (Proce2->CacheMatrix[IndexData][i]->Tag == TagData) {
						StoreHit = true;
						Proce2->CacheMatrix[IndexData][i]->LRU = false;
						if (i == 0 && Proce2->CacheMatrix[IndexData][i + 1] != nullptr) {
							Proce2->CacheMatrix[IndexData][i + 1]->LRU = true;
						}
						else if (i == 1 && Proce2->CacheMatrix[IndexData][i + 1] != nullptr) {
							Proce2->CacheMatrix[IndexData][i - 1]->LRU = true;
						}
						CheckBusOnWrite(Proce2, Proce1, i, HitCol1, E1);
						break;
					}
				}
			}
			if (!StoreHit) {
				Proce2->Misses++;
				bool WriteHit = SharedCache->L2Check();
				if (WriteHit == false) {
					SharedCache->Misses++;
					Campo1* Evicted = Proce2->Evict();
					SharedCache->CacheMatrix[SharedCache->IndexData]->Tag = SharedCache->TagData;
					SharedCache->CacheMatrix[SharedCache->IndexData]->Coherence = 4;
					if (Evicted != nullptr) {
						SharedCache->Evict(Evicted);
					}
				}
				else {
					Campo1* Evicted = Proce2->Evict();
					if (Evicted != nullptr) {
						SharedCache->Evict(Evicted);
					}
				}
			}

		}
	}

}