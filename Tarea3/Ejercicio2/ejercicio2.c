#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include<math.h>
//Funcion que se encarga de imprimir una matriz
void ImprimirMatriz(int filas, int columnas, int matriz[filas][columnas])
{
    int i, j;
    for (i = 0; i < filas; i++)
    {
        for (j = 0; j < columnas; j++)
        {
            printf(" %d", matriz[i][j]);
        }
        printf("\n");
    }
}
//Funcion que se encarga de imprimir de generar una  matriz
void GenerarMatriz(int filas, int columnas, int mat[filas][columnas])
{
    int x, y;
    for(x = 0; x < filas; x++)
    {
        for(y = 0; y < columnas; y++)
            mat[x][y] =x+y;
    }
}

int main(int argc, char *argv[]){

  int rank, size;
  int filas,columnas;
  MPI_Init(&argc, &argv);
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  MPI_Comm_size(MPI_COMM_WORLD, &size);

  if(rank == 0){
    clock_t begin = clock();
    //El tamano de las filas y columnas los lee desde terminal y los asigna como corresponde
    filas=atoi(argv[1]);
    columnas=atoi(argv[1]);
    int vector[1][columnas];
    int matriz[filas][columnas];
    int Resultado[filas][1];
    //Genera el imprime la matriz a multiplicar
    GenerarMatriz(filas, columnas, matriz);
    printf("Matriz: \n");
    ImprimirMatriz(filas, columnas, matriz);
    //Genera e imprime  el vector como una amtriz de una sola fila
    GenerarMatriz(1, columnas, vector);
    printf("Vector: \n");
    ImprimirMatriz(1, columnas, vector);

    //Envia la cantidad de filas y columnas a todos los procesadores
    MPI_Bcast(&filas, 1, MPI_INT, 0, MPI_COMM_WORLD);
    MPI_Bcast(&columnas,1, MPI_INT, 0, MPI_COMM_WORLD);
    //Envia el vector a todos los procesadores
    MPI_Bcast(vector[0], columnas, MPI_INT, 0, MPI_COMM_WORLD);

    int bandera;
    int ResultadoTemp;
    int vtemp = 1;
    MPI_Status status;
    //For que recorre las filas para el resultado
    for(int numero_fila=0; numero_fila<filas;numero_fila++){

      MPI_Recv(&bandera,1, MPI_INT, MPI_ANY_SOURCE, 0, MPI_COMM_WORLD, &status);
      //Manda por cual fila va recorriendo la matriz
      vtemp = numero_fila;
      MPI_Send(&vtemp, 1, MPI_INT, status.MPI_SOURCE, 1, MPI_COMM_WORLD);
      //manda la fila a multiplicar
      MPI_Send(matriz[numero_fila], columnas, MPI_INT, status.MPI_SOURCE, 2, MPI_COMM_WORLD);
      //Recive los resultados y los guarda en Resultado
      MPI_Recv(&ResultadoTemp,1, MPI_INT, MPI_ANY_SOURCE, 3, MPI_COMM_WORLD, &status);
      Resultado[numero_fila][0] = ResultadoTemp;

    }
    //manda un mensaje para que los procesadores dejen de trabajar
    vtemp = -1;
    for(int process=1; process<size; process++){
      MPI_Send(&vtemp, 1, MPI_INT, process, 1, MPI_COMM_WORLD);
    }
    //Imprime el resultado
    printf("Resultado: \n");
    ImprimirMatriz(filas, 1, Resultado);
    clock_t end = clock();
    double time_spent = (double)(end - begin) / CLOCKS_PER_SEC;
    printf(" tiempo  de ejecucion :%f \n", time_spent);
}else{
  int columnas, filas;
  //recibe el tamano de la matriz y vector
  MPI_Bcast(&columnas, 1, MPI_INT, 0, MPI_COMM_WORLD);
  MPI_Bcast(&filas, 1, MPI_INT, 0, MPI_COMM_WORLD);
  //genera los vectores a multiplicar
  int *vector;
  vector = (int *)malloc(columnas*sizeof(int));
  MPI_Bcast(vector, columnas, MPI_INT, 0, MPI_COMM_WORLD);

  int *fila;
  fila = (int *)malloc(columnas*sizeof(int));
  int bandera;
  int vtemp = 1;

  while(1){
    MPI_Send(&vtemp, 1, MPI_INT, 0, 0, MPI_COMM_WORLD);
    MPI_Recv(&bandera,1, MPI_INT, 0, 1, MPI_COMM_WORLD, MPI_STATUS_IGNORE);

    if(bandera == -1){
      break;
    }
    else{
      //Recibe los valores y realiza los calculos
      MPI_Recv(fila, columnas, MPI_INT, 0, 2, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
      int Resultado_entry = 0;

      for(int i=0;i<columnas; i++){
        Resultado_entry = Resultado_entry + fila[i]*vector[i];
      }

      MPI_Send(&Resultado_entry, 1, MPI_INT, 0, 3, MPI_COMM_WORLD);
      //Imprime lo que hizo
      printf("Soy el procesador %d y obtuve: %d\n", rank, Resultado_entry);

    }
  }

  free(fila);
  free(vector);
}

MPI_Finalize();

return 0;

}
